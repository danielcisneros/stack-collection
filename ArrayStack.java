
/**
 * An array implementation of a stack in which the 
 * bottom of the stack is fixed at index 0.
 * 
 * @author Daniel Cisneros
 * @version 1.0
 * 
 */

import java.util.EmptyStackException;
import java.util.Arrays;

public class ArrayStack<T> implements StackADT<T> {
    private final static int DEFAULT_CAPACITY = 100;

    private int top; //The index of the top element in our stack.
    private T[] stack;

    /**
     * Creates an empty stack using the default capacity
     */
    public ArrayStack() {
        this(DEFAULT_CAPACITY);
    }

    /**
     * Creates an empty stack using the specifed capacity
     * @param initialCapacity the inital size of the array.
     */
    public ArrayStack(int initialCapacity) {
        //Top tells us the number of elements in the stack. Instead we want it to 
        //tell us which index points to the top element.
        //
        top = -1;
        stack = (T[]) (new Object[initialCapacity]);
    }

    /**
     * Adds the specified element to the top of the stack,
     * expanding the capacity of the array if necessary.
     * @param element generic element to be pushed onto the stack.
     */
    public void push(T element) {
        if (size() == stack.length) {
            expandCapacity();
        }
        top++;
        stack[top] = element;
    }

    /**
     * Remove the element at the top of this stack and returns
     * a reference to it.
     * @return element removed from the top of the stack
     * @throws EmptyCollectionException if stack is empty.
     */
    public T pop() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyStackException("stack");
        }
        T result = stack[top];
        stack[top] = null;
        top--;

        return result;
    }

    /**
     *  Creates a new array to store the contents of this stack
     *  with twice the capacity of the old one.
     */
    private void expandCapacity() {
        stack = Arrays.copyOf(stack, stack.length * 2);
    }

    /**
     * Returns a reference to the element at the top of this stack.
     * The element is not removed from the stack. Peek
     * @return element on top of the stack.
     * @throws EmptyCollectionException if the stack is empty.
     */
    public T peek() throws EmptyStackException {
        if (isEmpty()) {
            throw new EmptyStackException("stack");
        }
        return stack[top];
    }

    /**
     * Returns true if this stack is empty and false otherwise.
     * @return true if this stack is empty
     */
    public boolean isEmpty() {
        return (top == -1);
    }

    /**
     * Returns the number of elements in this stack.
     * @return size the number of elements in the stack.
     */
    public int size() {
        return top + 1;
    }

    /**
     * Returns a string representation of this stack.
     * @return a string representation of the stack
     */
    public String toString() {
        String result = "";

        for (int index = 0; index <= top; index++) {
            result = result + stack[index] + "\n";
        }
        return result;
    }
}