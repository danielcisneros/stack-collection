/**
 * Defines the interface to a stack collection
 * 
 * @author Daniel Cisneros
 * @version 1.0
 */

public interface StackADT<T>{
    /**
     * Adds the specified element to the top of the stack.
     * @param element element to be pushed onto the stack
     */
    public void push(T element);

    /**
     * Removes and returns the top element of this stack.
     * @return the element removed from the stack.
     */
    public T pop();

    /**
     * Return without removing the top element of this stack
     * @return the element removed from the stack
     */
    public T peek();

    /**
     * Returns true if this stack is empty and false otherwise.
     * @return true if this stack is empty
     */
    public boolean isEmpty();

    /**
     * Returns the number of elements in this stack.
     * @return size the number of elements in the stack.
     */
    public int size();

    /**
     * Returns a string representation of this stack.
     * @return a string representation of the stack
     */
    public String toString();
}