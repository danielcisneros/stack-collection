/**
 * Solution to Programming Project 12.5
 * 
 * @author Daniel Cisneros
 * @version 1.0
 */

public class ArrayDropOutStack<T> implements StackADT<T> {
    private final static int DEFAULT_CAPACITY = 100;

    private int top, bottom, count;
    private T[] stack;

    /**
     * Creates an empty stack using the default capacity.
     */
    public ArrayDropOutStack() {
        this(DEFAULT_CAPACITY);
    }

    /**
     * Creates an empty stack using the specified capacity.
     * @param initialCapacity represents the specified capacity.
     */
    public ArrayDropOutStack(int initialCapacity) {
        top = -1;
        bottom = 0;
        count = 0;
        stack = (T[]) (new Object[initialCapacity]);
    }

    /**
     * Adds the specified element to the top of this stack, expanding 
     * the capacity of the stack if neccessary.
     * @param element genereic element to be pushed onto the stack
     */
    public void push(T element) {
        if (size() == stack.length) {
            bottom = (bottom + 1) % stack.length;
        }

        top = (top + 1) % stack.length;
        stack[top] = element;
        count++;
    }

    /**
     * Removes the element at the top of this stack and returns a
     * reference to it. Throws and EmptyCollectionException if the stack
     * is empty.
     * @return T element removed from top of stack
     * @throws EmptyCollectionException if a pop is attempted on empty stack
     */
    public T pop() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("Drop Out Stack");
        }

        T result = stack[top];
        stack[top] = null;
        if (top == 0) {
            top = stack.length - 1;
        } else {
            top--;
        }

        return result;
    }

    /**
     * Returns a reference to the element at the top of this stack.
     * The element is not removed from the stack. Throws an
     * EmptyCollectionException if the stack is empty.
     * @return T element on top of the stack
     * @throws EmptyCollectionException if a peek is attempted on empty stack
     */
    public T peek() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("Drop out Stack");
        }

        return stack[top];
    }

    /**
     * Returns true if this stack is empty and false otherwise
     * @return boolean true if the stack is empty, flase otherwise
     */
    public boolean isEmpty() {
        return (count == 0);
    }

    /**
     * Returns the number of elements in this stack.
     * @return int the number of elements in this stack
     */
    public int size() {
        return count;
    }

    /**
     * Returns a string representation of this stack.
     * @return String representation of this stack
     */
    public String toString() {
        String result = "";

        for (int scan = 0; scan < top; scan++) {
            result = result + stack[scan].toString() + "\n";
        }
        
        return result; 
    }
}